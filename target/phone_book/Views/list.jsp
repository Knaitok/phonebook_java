<%@ page import="java.util.List" %>
<%@ page import="app.entities.tableBD" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Телефонная книга</title>
</head>

<body>
<h1>Телефонная книга</h1>
<%
    if (request.getAttribute("userName") != null) {
        out.println("<div>"+
                "<h5>" + request.getAttribute("userName") + "</h5>\n" +
                "</div>");
    }
%>

<div>
    <div>
        <div class="row">
            <div class="col">
                <%if (request.getSession().getAttribute("login")!=null){
                    out.println("<button class='btn btn-primary' onclick=location.href='/add'>Добавить номер</button>");
                }%>
            </div>
            <div class="col-6">
            <form method="post" class="form-inline">
                <input class="form-control col-6"  type="search" name ="search">
                <button class="btn btn-outline-success" type="submit">Поиск</button>
            </form>
            </div>
            <div class="col">
        <button class="btn btn-primary" onclick="location.href='/'">Очистить</button>
            </div>
            <div class="col">
                <%if (request.getSession().getAttribute("login")!=null){
                    out.println(request.getSession().getAttribute("login"));
                    out.println("<a href='/authorization'>Выход</a>");
                }else{
                    out.println("<button class='btn btn-primary' onclick=location.href='/authorization'>Авторизация</button>");
                }%>
            </div>
        </div>
    <table class="table table-striped">
        <tr>
            <th scope="col">Название</th>
            <th scope="col">Номер телефона</th>
            <th scope="col"></th>
            <th scope="col"></th>
        </tr>
        <%
            List<tableBD> select = (List<tableBD>) request.getAttribute("select");
            if (select != null && !select.isEmpty()) {
                for (tableBD s : select) {
                    out.println("<tr>" +
                     "<td>"+s.getUsername() +"</td>"+
                     "<td>"+s.getUsernumber() +"</td>"+
                     "<td><form method='post' action='/update'>"+
                        "<input type='hidden' name='id' value='"+s.getId()+"'>"+
                        "<input type='hidden' name='name' value='"+s.getUsername()+"'>"+
                        "<input type='hidden' name='number' value='"+s.getUsernumber()+"'>"+
                        "<input class='btn btn-primary' type='submit' value='Изменить'>"+
                     "</form></td>"+
                     "<td><form method='post' action='/delete'>"+
                        "<input type='hidden' name='id' value='"+s.getId()+"'>"+
                        "<input class='btn btn-primary' type='submit' value='Удалить'>"+
                     "</td>"+
                     "</form></tr>");
                }
            } else out.println("<p>Телефонная книга пуста</p>");

        %>
    </table>
    </div>
</div>

</body>
</html>
