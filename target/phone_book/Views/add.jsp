<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Добавить пользователя</title>
</head>

<body >

<div >
    <%
        if (request.getAttribute("userName") != null) {
            out.println("<div>"+
                    "<h5>" + request.getAttribute("userName") + "</h5>\n" +
                    "</div>");
        }
    %>
    <div class="col-6">
        <div>
            <h2>Добавить пользователя</h2>
        </div>
        <form method="post">
            <div class="form-group">
                <label for="name">Имя</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="number">Номер телефона</label>
                <input type="text" class="form-control" id="number" name="number">
            </div>
            <button type="submit" class="btn btn-primary">Добавить</button>
        </form>
        <button class="btn btn-primary" onclick="location.href='/'">Вернуться к списку</button>
    </div>
</div>
<div >

</div>
</body>
</html>