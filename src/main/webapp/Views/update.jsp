<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Изменить номер</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
</head>
<body >
<div >

    <div class="col-6">
        <div >
            <h2>Изменить пользователя</h2>
        </div>
        <%
            if (request.getAttribute("userName") != null) {
                out.println("<div>"+
                        "<h5>" + request.getAttribute("userName") + "</h5>\n" +
                        "</div>");
            }
        %>
        <form method="post">
            <div class="form-group">
                <label for="name">Имя</label>
                <input type="text" name="name" class="form-control" id="name" value="<%=request.getAttribute("name")%>"><br />
            </div>
            <div class="form-group">
            </label>
                <label for="number">Номер телефона</label>
                <input name="number" class="form-control" id="number" name="number" value="<%=request.getAttribute("number")%>"><br />
            </label>
            </div>
                <input type ="hidden" name="id" value="<%=request.getAttribute("id")%>"><br />
            <button type="submit" class="btn btn-primary">Изменить</button>
        </form>
        <button class="btn btn-primary" onclick="location.href='/'">Вернуться к списку</button>
    </div>
</div>
</body>
</html>
