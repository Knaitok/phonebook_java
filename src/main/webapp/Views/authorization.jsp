
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>Title</title>
</head>
<body>

<div class="col-6">
    <div>
        <h2>Авторизация</h2>
    </div>
    <%
        if (request.getAttribute("userName") != null) {
            out.println("<div>"+
                    "<h5>" + request.getAttribute("userName") + "</h5>\n" +
                    "</div>");
        }
    %>
    <form method="post">
        <div class="form-group">
            <label for="login">Логин</label>
            <input type="text" class="form-control" id="login" name="login">
        </div>
        <div class="form-group">
            <label for="number">Пароль</label>
            <input type="password" class="form-control" id="number" name="password">
        </div>
        <button type="submit" class="btn btn-primary">Авторизоваться</button>
    </form>
</div>
</body>
</html>
