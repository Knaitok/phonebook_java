package app.servlets;

import app.model.bd;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class listServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String login = (String) session.getAttribute("login");
        String password = (String) session.getAttribute("password");
        bd select = new bd(login,password);
        req.setAttribute("select", select.select());
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("Views/list.jsp");
        requestDispatcher.forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        String searchString = req.getParameter("search");
        HttpSession session = req.getSession();
        String login = (String) session.getAttribute("login");
        String password = (String) session.getAttribute("password");
        bd search = new bd(login, password);
        req.setAttribute("select", search.Search(searchString));
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("Views/list.jsp");
        requestDispatcher.forward(req, resp);

    }
}
