package app.servlets;

import app.model.bd;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class deleteServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        String id = req.getParameter("id");
        HttpSession session = req.getSession();
        String login = (String) session.getAttribute("login");
        String password = (String) session.getAttribute("password");
        bd delete = new bd(login, password);

        if(delete.Delete(Integer.valueOf(id))){
            resp.sendRedirect("/");
        }else{
            req.setAttribute("userName", "Произошла ошибка удаления");
        }




    }
}
