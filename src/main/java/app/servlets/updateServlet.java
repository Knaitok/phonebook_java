package app.servlets;

import app.entities.tableBD;
import app.model.bd;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class updateServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("Views/update.jsp");
        req.setCharacterEncoding("utf8");
        req.setAttribute("name",req.getParameter("name"));
        req.setAttribute("number",req.getParameter("number"));
        req.setAttribute("id",req.getParameter("id"));
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String number = req.getParameter("number");
        HttpSession session = req.getSession();
        String login = (String) session.getAttribute("login");
        String password = (String) session.getAttribute("password");
        bd update = new bd(login, password);
        tableBD value = update.SearchId(Integer.valueOf(id));
        if(value !=null){
            if(Integer.valueOf(id).equals(value.getId())&&
                    name.equals(value.getUsername())&&
                    number.equals(value.getUsernumber())){
                requestDispatcher.forward(req, resp);
            }else{
                if(update.Update(Integer.valueOf(id),name, number)){
                    resp.sendRedirect("/");
                }else{
                    req.setAttribute("userName", "Не удалось изменить значения");
                    requestDispatcher.forward(req, resp);
                }
            }
        }
    }
}
