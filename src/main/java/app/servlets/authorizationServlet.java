package app.servlets;

import app.model.bd;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

public class authorizationServlet extends HttpServlet {
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().removeAttribute("login");
        req.getSession().removeAttribute("password");
        RequestDispatcher requestDispatcher = req.getRequestDispatcher("Views/authorization.jsp");
        requestDispatcher.forward(req, resp);
    }
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf8");
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        bd con = new bd(login,password);
        if(con.connect() !=null){
            req.getSession().setAttribute("login",login);
            req.getSession().setAttribute("password", password);
            resp.sendRedirect("/");
        }else{
            req.setAttribute("userName", "Неверный логи или пароль");
            req.getRequestDispatcher("Views/authorization.jsp").forward(req, resp);
        }
    }
}
