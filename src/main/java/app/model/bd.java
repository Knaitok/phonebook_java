package app.model;

import app.entities.tableBD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class bd {

    private String login;
    private String password;

    public bd(String login, String password){
        this.login = login;
        this.password = password;
    }
    public Connection connect(){
        Connection conn = null;
        try {
            String url = "jdbc:mysql://localhost/phonebook?serverTimezone=Europe/Moscow&useSSL=false";
            Properties p=new Properties();
            p.setProperty("user",this.login);
            p.setProperty("password",this.password);
            Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
            conn = DriverManager.getConnection(url, p);

        } catch (Exception ex) {
            System.out.println(ex);
            return null;
        }
        return conn;
    }

    public String add(String name, String number){
        Connection connect = this.connect();
        try{
       String request = "INSERT INTO phonebook (Username,Usernumber) VALUES ('"+name+"','"+number+"')";
            Statement statement = connect.createStatement();
            statement.executeUpdate(request);
       connect.close();
        }
        catch (Exception ex){
            System.out.println(ex);
            return "Не удалось добавить номер телефона";
        }
        return "Номер успешно добавлен";
    }

    public Boolean Update(Integer id, String name, String number){
        Connection connect = this.connect();
        try{
            String request = "UPDATE phonebook SET Username = '"+name+"', Usernumber = '"+number+"' WHERE id ="+id+"";
            Statement statement = connect.createStatement();
            statement.executeUpdate(request);
            connect.close();
        }
        catch (Exception ex){
            System.out.println(ex);
            return false;
        }
        return true;
    }
    public Boolean Delete(Integer id){
        Connection connect = this.connect();
        try{
            String request = "DELETE from phonebook WHERE id = "+id+"";
            Statement statement = connect.createStatement();
            statement.executeUpdate(request);
            connect.close();
        }
        catch (Exception ex){
            return false;
        }
        return true;
    }
    public List<tableBD> Search(String str){
        List<tableBD> list = new ArrayList<>();
        Connection connect = this.connect();
        try{
            String request = "SELECT * FROM phonebook WHERE Username LIKE '%"+str+"%'|| Usernumber LIKE '%"+str+"%'";
            Statement statement = connect.createStatement();
            ResultSet rs = statement.executeQuery(request);
            while (rs.next ()) {
                tableBD row = new tableBD (
                        rs.getInt ("id"),
                        rs.getString ("Username"),
                        rs.getString ("Usernumber")
                );
                list.add(row);
            }
            connect.close();
        }
        catch (Exception ex){
            System.out.println(ex);
        }
        return list;
    }
    public tableBD SearchId(Integer id){
        Connection connect = this.connect();
        try{
        String request = "SELECT * from phonebook WHERE id = "+id+"";
        Statement statement = connect.createStatement();
        ResultSet rs = statement.executeQuery(request);
        while (rs.next()) {
            tableBD row = new tableBD(
                    rs.getInt("id"),
                    rs.getString("Username"),
                    rs.getString("Usernumber")
            );
            connect.close();
            return row;
        }
        }
        catch (Exception ex){
            System.out.println(ex);
        }
        return null;
    }

    public List<tableBD> select(){
        List<tableBD> list = new ArrayList<>();
        Connection connect = this.connect();
        try{
            String request = "SELECT * FROM phonebook";
            Statement statement = connect.createStatement();
            ResultSet rs = statement.executeQuery(request);
            while (rs.next ()) {
                tableBD row = new tableBD (
                        rs.getInt ("id"),
                        rs.getString ("Username"),
                        rs.getString ("Usernumber")
                );
                list.add(row);
            }
            connect.close();
        }
        catch (Exception ex){
            System.out.println(ex);
        }
        return list;
    }


}
