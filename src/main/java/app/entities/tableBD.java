package app.entities;

public class tableBD {
    private int id;
    private String Username;
    private String Usernumber;

    public tableBD(Integer id, String Username, String Usernumber){
        this.id = id;
        this.Username = Username;
        this.Usernumber = Usernumber;
    }

    public String getUsernumber() {
        return Usernumber;
    }

    public String getUsername() {
        return Username;
    }

    public Integer getId(){
        return id;
    }
}
